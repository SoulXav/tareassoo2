PROGRAMA = tarea2

# Archivos fuente
FUENTES = codigo.cpp

# Opciones de compilación
CC = gcc
CFLAGS = -Wall -Wextra -Werror

# Opciones de enlazado
LD = gcc
LDFLAGS =

# Regla para compilar todo
all: $(PROGRAMA)

# Regla para compilar el programa
$(PROGRAMA): $(FUENTES)
    $(CC) $(CFLAGS) -o $@ $^

# Regla para limpiar
clean:
    rm -f $(PROGRAMA)
