#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char ip[16]; // Cadena para almacenar la dirección IP leída del archivo
    char comando[32]; // Cadena para almacenar el comando ping a ejecutar
    char buffer[128]; // Cadena para almacenar la salida del comando ping
    FILE *fp; // Puntero al archivo
    int paquetes_transmitidos, paquetes_recibidos, paquetes_perdidos; // Cantidad de paquetes transmitidos, recibidos y perdidos
    char estado[4]; // Estado de la dirección IP (UP o DOWN)

    // Abrir el archivo en modo de lectura
    fp = fopen("ip.txt", "r");
    if (fp == NULL) {
        printf("Error al abrir el archivo\n");
        return 1;
    }

    // Leer cada línea del archivo y obtener el reporte de salida de la dirección IP
    while (fscanf(fp, "%s", ip) != EOF) {
        // Construir el comando ping a ejecutar
        sprintf(comando, "ping -q %s", ip);

        // Ejecutar el comando ping y obtener el resultado
        fp = popen(comando, "r");
        if (fp == NULL) {
            printf("Error al ejecutar el comando ping\n");
            return 1;
        }

        // Leer la salida del comando ping
        fgets(buffer, 128, fp);
        while (fgets(buffer, 128, fp) != NULL) {

            sscanf(buffer, "%d packets transmitted, %d received, %d%% packet loss", &paquetes_transmitidos, &paquetes_recibidos, &paquetes_perdidos);
        }

        pclose(fp);

        // Determinar el estado de la dirección IP
        if (paquetes_perdidos == 100) {
            strcpy(estado, "DOWN");
        } else {
            strcpy(estado, "UP");
        }

        // Mostrar el reporte de salida
        printf("Numero IP: %s\n", ip);
        printf("Paquetes Transmitidos: %d\n", paquetes_transmitidos);
        printf("Paquetes Recibidos: %d\n", paquetes_recibidos);
        printf("Paquetes Perdidos: %d\n", paquetes_perdidos);
        printf("Estado: %s\n", estado);
    }

    // Cerrar el archivo
    fclose(fp);

    return 0;
}

